package com.chat;

import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Scanner;

import org.springframework.stereotype.Component;


@Component
public class OneSignal {
	
	
	public int sendMessageNotification(MessagePostToken message,int userToId){
		int status = 400;
		try {
			   String jsonResponse;
			   
			   URL url = new URL("https://onesignal.com/api/v1/notifications");
			   HttpURLConnection con = (HttpURLConnection)url.openConnection();
			   con.setUseCaches(false);
			   con.setDoOutput(true);
			   con.setDoInput(true);

			   con.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
			   con.setRequestProperty("Authorization", "Basic MTNlNDU2NmMtMjRkZi00YmNhLTk4ODItNzE5Nzg2NTY2YjUy");
			   con.setRequestMethod("POST");


			   String strJsonBody = "{"
			                      +   "\"app_id\": \"0ec909e8-312e-4e96-bf45-b26dd4529333\","
			                      //+   "\"included_segments\": [\"All\"],"
			                      + "\"headings\":{\"en\": \"TuDate\"},"
			                      + "\"small_icon\": \"https://s21.postimg.org/lry253pmv/Tu_Date_Icon.png\","
			                      +   "\"tags\": [{\"key\":\"user_id\",\"relation\":\"=\",\"value\":"+userToId+"}],"
			                      +   "\"data\": {\"type\":\"message\","
			                      + "\"userFrom\":"+message.getMessage().getUserId()+","
			                      + "\"name\":\""+message.getMessage().getName()+" "+message.getMessage().getSurname()+"\","
			                      + "\"username\":\""+message.getMessage().getUsernameFrom()+"\","
			                      + "\"message\":\"\"},"
			                      +   "\"contents\": {\"en\": \""+message.getMessage().getName()+" "+message.getMessage().getSurname()+": "+message.getMessage().getContent()+"\"}"
			                      + "}";
			         
			   
			   System.out.println("strJsonBody:\n" + strJsonBody);

			   byte[] sendBytes = strJsonBody.getBytes("UTF-8");
			   con.setFixedLengthStreamingMode(sendBytes.length);

			   OutputStream outputStream = con.getOutputStream();
			   outputStream.write(sendBytes);

			   int httpResponse = con.getResponseCode();
			   System.out.println("httpResponse: " + httpResponse);
               status = httpResponse;
			   if (  httpResponse >= HttpURLConnection.HTTP_OK
			      && httpResponse < HttpURLConnection.HTTP_BAD_REQUEST) {
			      Scanner scanner = new Scanner(con.getInputStream(), "UTF-8");
			      jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
			      scanner.close();
			   }
			   else {
			      Scanner scanner = new Scanner(con.getErrorStream(), "UTF-8");
			      jsonResponse = scanner.useDelimiter("\\A").hasNext() ? scanner.next() : "";
			      scanner.close();
			   }
			   System.out.println("jsonResponse:\n" + jsonResponse);
			   
			} catch(Throwable t) {
			   t.printStackTrace();
			}
		return status;
	}
	
}
