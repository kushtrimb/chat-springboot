package com.chat;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.config.annotation.EnableWebSocket;
import org.springframework.web.socket.config.annotation.WebSocketConfigurer;
import org.springframework.web.socket.config.annotation.WebSocketHandlerRegistry;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import com.fasterxml.jackson.databind.ObjectMapper;

@Configuration
@EnableWebSocket
public class WebSocketConfig implements WebSocketConfigurer {

	@Autowired
	private UsersInSession userInSession;
	@Autowired
	private JDBCConnection jdbcConnection;
	@Autowired
	private BCrypt bcrypt;
	@Autowired
	private OneSignal one;

	private int test;

	@Override
	public void registerWebSocketHandlers(WebSocketHandlerRegistry wsh) {
		wsh.addHandler(new TuDateHandler(), "/tudate").setAllowedOrigins("*");
	}

	class TuDateHandler extends TextWebSocketHandler {

		@Override
		public void afterConnectionEstablished(WebSocketSession session) throws Exception {
			System.out.println(session.getUri());
			String name = session.getUri().toString().split("=")[1];
			userInSession.getUsers().put(session, name);
			ArrayList<WebSocketSession> sessionsForUser = userInSession.getUsersComp().get(name);
			if (sessionsForUser == null) {
				sessionsForUser = new ArrayList<>();
			}
			for (WebSocketSession ss : sessionsForUser) {
				if (!ss.isOpen()) {
					System.out.println("..........removing closed sessions............");
					sessionsForUser.remove(ss);
				}
			}
			System.out.println(sessionsForUser);
			sessionsForUser.add(session);
			userInSession.getUsersComp().put(name, sessionsForUser);
		}

		@Override
		public void handleTextMessage(WebSocketSession session, TextMessage message) throws Exception {

			System.out.println(test++);
			Connection conn = null;
			PreparedStatement stm = null;
			PreparedStatement ps = null;
			PreparedStatement pstm = null;
			ResultSet rs = null;

			try {
				String name = userInSession.getUsers().get(session);
				System.out.println("from: " + name);
				ObjectMapper mapper = new ObjectMapper();
				// System.out.print(message.getPayload());
				MessagePostToken messageObj = mapper.readValue(message.getPayload(), MessagePostToken.class);
				String usernameTo = messageObj.getMessage().getUsernameTo();

				MessagePostToSend msgPost = new MessagePostToSend();
				MessageObjectToSend msgSend = new MessageObjectToSend();
				msgSend.setContent(messageObj.getMessage().getContent());
				msgSend.setName(messageObj.getMessage().getName());
				msgSend.setSurname(messageObj.getMessage().getSurname());
				msgSend.setUserId(messageObj.getMessage().getUserId());
				msgSend.setPhotoUrl(messageObj.getMessage().getPhotoUrl());
				msgPost.setMessage(msgSend);

				String send = mapper.writeValueAsString(msgPost);

				System.out.println("-------------------------------------");
				System.out.println(usernameTo);
				System.out.println("-------------------------------------");

				TextMessage messageSend = new TextMessage(send);

				/*
				 * 
				 * boolean isConnected =
				 * userInSession.getUsersComp().get(messageObj.getMessage().
				 * getUsernameTo()) != null;
				 * 
				 * final String uri =
				 * "http://localhost:8081/TuDate/api/sendMessageFromWs/?access_token="
				 * + messageObj.getMessage().getToken();
				 * 
				 * MessagePost mp = new MessagePost(); MessageObject mo = new
				 * MessageObject();
				 * mo.setUsernameFrom(messageObj.getMessage().getUsernameFrom())
				 * ; mo.setContent(messageObj.getMessage().getContent());
				 * mo.setUsernameTo(messageObj.getMessage().getUsernameTo());
				 * mo.setConnected(isConnected); mp.setMessage(mo);
				 * 
				 * RestTemplate restTemplate = new RestTemplate();
				 * //restTemplate.postForObject(uri,
				 * mapper.writeValueAsString(mp), String.class);
				 */
				SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
				Date date = new Date();
				String data = formatter.format(date);
				String key = bcrypt.generateSecretKey();
				String mesazh = bcrypt.encryptMessage(messageObj.getMessage().getContent(), key);

				String sql = "SELECT online,id FROM user WHERE username = ?";
				
				
				
			    conn = jdbcConnection.getJdbcDataSource().getConnection();
			    
				stm = conn.prepareStatement(sql);
				stm.setString(1, messageObj.getMessage().getUsernameTo());
				
				
				
				rs = stm.executeQuery();
				int flag = 0;
				int userToid = 0;
				// STEP 5: Extract data from result set
				while (rs.next()) {
					flag = rs.getInt("online");
					userToid = rs.getInt("id");
				}
				
				String block = "SELECT id FROM block_user WHERE user_from = ? and user_to = ?";
				pstm = conn.prepareStatement(block);
			    pstm.setInt(1, userToid);
			    pstm.setInt(2, messageObj.getMessage().getUserId());
			    
			    rs = pstm.executeQuery();
			    boolean blocked = false;
			    while (rs.next()) {
			    	blocked = true;
					System.out.print("Bllockeeeeeeeeeeeeeeeeeeeeeeed!!!!!!!!");
				}
			    
			    if(blocked){
			    	session.sendMessage(new TextMessage("{\"status\":\"Blocked\"}"));
			    }else{
				
				System.out.print("Online: " + test++ + "flag: " + flag);
				StringBuilder sb = new StringBuilder();
				sb.append("INSERT INTO MESSAGE");
				sb.append("(from_user, to_user, content, created_date, keygen,flag) ");
				sb.append("VALUES (?,?,?,?,?,?);");
				
				/*sb.append("  '" + messageObj.getMessage().getUserId() + "'");
				sb.append(", '" + userToid + "' ");
				sb.append(", '" + mesazh + "' ");
				sb.append(", '" + data + "' ");
				sb.append(", '" + key + "'");
				sb.append(", '" + flag + "'");
				sb.append(")");*/

				ps = conn.prepareStatement(sb.toString());
				ps.setInt(1, messageObj.getMessage().getUserId());
				ps.setInt(2, userToid);
				ps.setString(3, mesazh);
				ps.setString(4, data);
				ps.setString(5, key);
				ps.setInt(6, flag);
				
				int exc = ps.executeUpdate();
				
				System.out.print("rows efected: " + exc);
				int status = one.sendMessageNotification(messageObj, userToid);

				System.out.println("Status : " + status);

				ArrayList<WebSocketSession> ss = userInSession.getUsersComp().get(usernameTo);
				if (ss != null) {
					for (WebSocketSession sess : ss) {
						if (sess.isOpen()) {
							System.out.println("%%%%%%%%%%%%%%%");
							System.out.println("Is Session Opened: " + sess.isOpen());
							System.out.println("Session Id: " + sess.getId());
							System.out.println("%%%%%%%%%%%%%%%");
							sess.sendMessage(messageSend);
							session.sendMessage(new TextMessage("{\"status\":\"OK\"}"));
							System.out.println("====== Users Nr =======:" + ss.size());
							System.out.println("=======Sssion Id=======:" + sess.getId());
						} else {
							System.out.println("====== Users Session Closed =======:" + sess.isOpen());
							System.out.println("=======Sssion Id=======:" + sess.getId());
						}
					}
				}
			  }
			} catch (SQLException se) {
				se.printStackTrace();
			} finally {
				System.out.print("closing connection..............");
				try {
					if (stm != null)
						conn.close();
					if (ps != null)
						ps.close();
				} catch (SQLException se) {
				} // do nothing
				try {
					if (conn != null)
						System.out.println("Max Active: " + jdbcConnection.getJdbcDataSource().getMaxActive());
					conn.close();
					System.out.println("Max Active: " + jdbcConnection.getJdbcDataSource().getMaxActive());
				} catch (SQLException se) {
					se.printStackTrace();
				} // end finally try

			}

			/*
			 * URL url = new URL(uri); HttpURLConnection conn =
			 * (HttpURLConnection) url.openConnection(); conn.setDoOutput(true);
			 * conn.setRequestMethod("POST");
			 * conn.setRequestProperty("Content-Type", "application/json");
			 * 
			 * String input = mapper.writeValueAsString(mp);
			 * 
			 * OutputStream os = conn.getOutputStream();
			 * os.write(input.getBytes()); os.flush();
			 * 
			 * System.out.println(conn.getResponseCode());
			 * System.out.println("---------------------------");
			 * //System.out.println(result.getDescription());
			 * System.out.println(uri);
			 * System.out.println(mapper.writeValueAsString(mp));
			 * System.out.println("---------------------------");
			 * 
			 * System.out.println(isConnected); //if
			 * (result.getDescription().equals("OK")) {
			 * 
			 * //} String status = "OK";
			 * 
			 * 
			 * if(conn.getResponseCode() == 200){ if(ss != null){
			 * 
			 * }else{ System.out.println(usernameTo+": Not in the session");
			 * //session.sendMessage(new TextMessage(status)); } }else{
			 * session.sendMessage(new TextMessage("{\"status\":\"Failed\"}"));
			 * }
			 */
		}

		@Override
		public void afterConnectionClosed(WebSocketSession session, CloseStatus status) throws Exception {
			String name = userInSession.getUsers().get(session);
			System.out.println("closing...... " + name);

			ArrayList<WebSocketSession> userat = userInSession.getUsersComp().get(name);
			if (userat != null) {
				for (int i = 0; i < userat.size(); i++) {
					if (userat.get(i).equals(session)) {
						System.out.print("close id: " + session.getId());
						userat.remove(i);
					}
				}
			}
			userInSession.getUsers().remove(session);
		}

	}

}