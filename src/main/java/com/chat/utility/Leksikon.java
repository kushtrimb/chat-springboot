package com.chat.utility;

import org.springframework.stereotype.Component;

@Component
public class Leksikon {
	
	private int questionId;

	public int getQuestionId() {
		return questionId;
	}

	public void setQuestionId(int questionId) {
		this.questionId = questionId;
	}

}
