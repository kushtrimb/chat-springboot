package com.chat;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

import org.apache.commons.dbcp.BasicDataSource;
import org.springframework.stereotype.Component;

@Component
public class JDBCConnection {
	
	private Connection conn = null;
	private Statement stmt = null;
	BasicDataSource dataSource = null;
	
	
	
	private String DB_URL = "jdbc:mysql://localhost:3306/tudate";
	private String USER = "root";
	private String PASS = "";
	
	public JDBCConnection(){
		
		dataSource = new BasicDataSource();
		dataSource.setDriverClassName("com.mysql.jdbc.Driver");
		dataSource.setUsername(USER);
		dataSource.setPassword(PASS);
		dataSource.setUrl(DB_URL);
		dataSource.setMaxActive(10);
		dataSource.setMaxIdle(5);
		dataSource.setInitialSize(5);
		dataSource.setValidationQuery("SELECT 1");
		
	}
	
	public Connection createConnection() throws ClassNotFoundException, SQLException{
		Class.forName("com.mysql.jdbc.Driver");
		conn = DriverManager.getConnection(DB_URL, USER, PASS);
		return conn;
	}
	
    public Connection getJdbcConnection() throws SQLException{
    	return dataSource.getConnection();
    }
    
    public BasicDataSource getJdbcDataSource() throws SQLException{
    	return dataSource;
    }
    
    public Statement getStatement() throws SQLException{
    	return conn.createStatement();
    }

}
