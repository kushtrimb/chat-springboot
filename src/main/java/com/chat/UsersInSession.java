package com.chat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.springframework.stereotype.Component;
import org.springframework.web.socket.WebSocketSession;

@Component
public class UsersInSession {

	private Map<WebSocketSession,String> users;
	private Map<String,ArrayList<WebSocketSession>> usersComp;
	
	public UsersInSession(){
		users = new HashMap<WebSocketSession,String>();
		usersComp = new HashMap<String,ArrayList<WebSocketSession>>();
	}

	public Map<WebSocketSession, String> getUsers() {
		return users;
	}

	public void setUsers(Map<WebSocketSession, String> users) {
		this.users = users;
	}

	public Map<String, ArrayList<WebSocketSession>> getUsersComp() {
		return usersComp;
	}

	public void setUsersComp(Map<String, ArrayList<WebSocketSession>> usersComp) {
		this.usersComp = usersComp;
	}

	
	
}
