package com.chat;

public class MessageObject {

    private String usernameFrom;
    private String usernameTo;
    private String content;
    private boolean isConnected;
    
	
	public String getContent() {
		return content;
	}
	public void setContent(String content) {
		this.content = content;
	}
	public boolean isConnected() {
		return isConnected;
	}
	public void setConnected(boolean isConnected) {
		this.isConnected = isConnected;
	}
	public String getUsernameFrom() {
		return usernameFrom;
	}
	public void setUsernameFrom(String usernameFrom) {
		this.usernameFrom = usernameFrom;
	}
	public String getUsernameTo() {
		return usernameTo;
	}
	public void setUsernameTo(String usernameTo) {
		this.usernameTo = usernameTo;
	}
	
   
}