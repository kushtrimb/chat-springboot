package com.chat;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Base64;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.spec.SecretKeySpec;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class BCrypt {
	
	
	public String generateSecretKey(){		
		KeyGenerator keyGen = null;
		try {
			keyGen = KeyGenerator.getInstance("AES");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		keyGen.init(128);
		SecretKey secretKey = keyGen.generateKey();
		return Base64.getEncoder().encodeToString(secretKey.getEncoded());
	}

	public String encryptPassword(String password) {

		MessageDigest messageDigest = null;
		try {
			messageDigest = MessageDigest.getInstance("MD5");
		} catch (NoSuchAlgorithmException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		messageDigest.update(password.getBytes(), 0, password.length());
		String hashedPass = new BigInteger(1, messageDigest.digest()).toString(16);
		if (hashedPass.length() < 32) {
			hashedPass = "0" + hashedPass;
		}
		return hashedPass;
	}

	public String encryptMessage(String msg, String secretKey) {
		byte[] decodedKey = Base64.getDecoder().decode(secretKey);
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
			String plainData = msg, cipherText=null;			
			try {
				cipherText = encrypt(plainData, originalKey);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return cipherText;
	}

	public String decryptMessage(String msg, String secretKey) {
		
		byte[] decodedKey = Base64.getDecoder().decode(secretKey);
		SecretKey originalKey = new SecretKeySpec(decodedKey, 0, decodedKey.length, "AES"); 
		
			String cipherText = msg, decryptedText = null;
			try {
				decryptedText = decrypt(cipherText, originalKey);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			return decryptedText;
	}

	private String encrypt(String plainData, SecretKey secretKey) throws Exception {
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.ENCRYPT_MODE, secretKey);
		byte[] byteDataToEncrypt = plainData.getBytes();
		byte[] byteCipherText = aesCipher.doFinal(byteDataToEncrypt);
		return Base64.getEncoder().encodeToString(byteCipherText);
	}

	private String decrypt(String cipherData, SecretKey secretKey) throws Exception {
		byte[] data = Base64.getDecoder().decode(cipherData);
		Cipher aesCipher = Cipher.getInstance("AES");
		aesCipher.init(Cipher.DECRYPT_MODE, secretKey);
		byte[] plainData = aesCipher.doFinal(data);
		return new String(plainData);
	}

}
