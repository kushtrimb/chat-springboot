var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect() {
    var socket = new SockJS('http://85.214.89.204:8080/tudate');
    stompClient = Stomp.over(socket);
    stompClient.connect({company: "kushtrim"}, function (frame) {
        setConnected(true);
        console.log('Connected: ' + frame);
        stompClient.subscribe('/user/kushtrim/reply', function (result) {
            console.log("====================");
            console.log(JSON.parse(result.body));
            console.log("====================");
            showGreeting(JSON.parse(result.body).message.content);
        });
    });
}

function disconnect() {
    if (stompClient != null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendMessage() {
    stompClient.send("/app/sendMessage", {}, 
            JSON.stringify({'message':{'usernameTo': $("#name").val(),
            	                       'usernameFrom':'fitim',
            	                       'content':'hello kushtrim!!',
            	                       'token':'6e41d4b2-b42d-4be9-bd52-43e537aa9582'}}));
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

$(function () {
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() { connect(); });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendMessage(); });
});

